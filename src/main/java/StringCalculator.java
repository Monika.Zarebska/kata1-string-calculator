public class StringCalculator {

    public int add(String input) throws InvalidFormatException {

        validateInput(input);

        if (input.equals("")) {
            return 0;
        }
        if (isOneNumber(input)) {
            return Integer.parseInt(input);
        }

        String[] numbers = input.split("[,\\n]");

        int result = 0;
        for (String stringNumber : numbers) {
            int number = Integer.parseInt(stringNumber);
            result = result + number;
        }
        return result;
    }

    private boolean isInvalidFormat(String input) {
        return input.contains(",\n");
    }

    private boolean isOneNumber(String input) {
        return input.matches("[0-9]");
    }

    private void validateInput(String input) {
        if (isInvalidFormat(input)) throw new InvalidFormatException();

        if (isNegativeNumber(input))
            throw new InvalidFormatException("negatives are not allowed: " + negativeNumber(input));
    }

    private boolean isNegativeNumber(String input) {
        return input.contains("-");
    }

    private String negativeNumber(String input) {
        String[] numbers = input.split("[,\\n]");
        String result = "";
        for (String number : numbers) {
            if (number.contains("-")) {
                result = result +","+ number;
            }
        }
        return result.substring(1);
    }
}
