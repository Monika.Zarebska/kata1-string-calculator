import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class StringCalculatorTest {

    StringCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new StringCalculator();
    }

    @Test
    void shouldReturnZeroForEmptyString() {
        int result = calculator.add("");

        assertThat(result).isEqualTo(0);
    }

    @Test
    void shouldReturnNumberWhenNumberGiven() {
        int result = calculator.add("1");

        assertThat(result).isEqualTo(1);
    }

    @Test
    void shouldReturnSumOfNumbersCommaSeparated() {
        int result = calculator.add("1,3,5,7");

        assertThat(result).isEqualTo(16);
    }

    @Test
    void shouldReturnSumOfNumberWhenNewLineSeparated() {
        int result = calculator.add("1\n2,3");

        assertThat(result).isEqualTo(6);
    }

    @Test
    void shouldThrowInvalidFormatExceptionWhenCommaIsFollowedByNewLine() {

        assertThatThrownBy(() ->
                calculator.add("1,\n")).isNotNull();
    }

    @Test
    void shouldThrowExceptionForNegativesNumbers() {

        assertThatThrownBy(() ->
                calculator.add("-1,-2,2\n3"))
                .hasMessageContaining("negatives are not allowed")
                .hasMessageContaining("-1")
                .hasMessageContaining("-2")
                .hasMessageNotContaining("3");
    }
}